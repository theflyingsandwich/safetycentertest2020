<?php

require "connection.php";

$dbConnection = getConnection();

$response = array();

$postReference 	= $_POST["postReference"];
$title 			= filter_var($_POST["title"], FILTER_SANITIZE_STRING);
$content 		= filter_var($_POST["content"], FILTER_SANITIZE_STRING);

$currentDate = getCurrentDate();

// UPDATE POST
$updatePostStatement = $dbConnection->prepare("UPDATE posts SET title=:title, content=:content, date_modified=:dateModified WHERE id = :postReference");	

try {

	$updatePostStatement->execute([
		'title' => $title,
		'content' => $content,
		'dateModified' => $currentDate,
		'postReference' => $postReference
	]);

	$response = array(
		"success" => true,
		"message" => "Post updated"
	);

} catch(Exception $e) {
	$response = array(
		"success" => false,
		"message" => $e->getMessage()
	);
}	
echo json_encode($response);