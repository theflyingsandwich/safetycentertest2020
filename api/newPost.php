<?php

require "connection.php";

$dbConnection = getConnection();

$response = array();

$userReference 	= $_POST["userReference"];
$title 		= filter_var($_POST["title"], FILTER_SANITIZE_STRING);
$content 	= filter_var($_POST["content"], FILTER_SANITIZE_STRING);

$currentDate = getCurrentDate();

// NEW POST
$newPostStatement = $dbConnection->prepare("INSERT into posts (user_reference, title, content, date_created) VALUES(:userReference, :title, :content, :dateCreated)");	

try {

	$newPostStatement->execute([
		'userReference' => $userReference,
		'title' => $title,
		'content' => $content,
		'dateCreated' => $currentDate
	]);

	$id = $dbConnection->lastInsertId();

	$response = array(
		"success" => true,
		"data" => $id,
		"message" => "Post created"
	);

} catch(Exception $e) {
	$response = array(
		"success" => false,
		"message" => $e->getMessage()
	);
}	
echo json_encode($response);