<?php

require "connection.php";

$dbConnection = getConnection();

$response = array();

$postId = $_GET["postId"];

if(isset($postId)) {
	//GET POST
	$getPostStatement = $dbConnection->prepare("SELECT p.*, u.id as user_id, u.username FROM posts p JOIN users u ON p.user_reference = u.id WHERE p.id = :postId AND p.status = 1");	
	$getPostStatement->execute(['postId' => $postId]);

	try {

		$post = $getPostStatement->fetch();

		$postArray = array(
			"postId" => $post["id"],
			"userReference" => $post["user_reference"],
			"username" => $post["username"],
			"title" => $post["title"],
			"content" => $post["content"],
			"dateCreated" => date("F j, Y | h:i A", strtotime($post["date_created"]))
		);
		
		$response = array(
			"success" => true,
			"data" => $postArray,
			"message" => "Post created"
		);

	} catch(Exception $e) {
		$response = array(
			"success" => false,
			"message" => $e->getMessage()
		);
	}	
}else{
	$response = array(
		"success" => false,
		"message" => $e->getMessage()
	);
}


echo json_encode($response);