<?php

require "connection.php";

$dbConnection = getConnection();

$response = array();

$email 		= filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);
$password 	= filter_var($_POST["password"], FILTER_SANITIZE_STRING);

$currentDate = getCurrentDate();

$user = $dbConnection->prepare("SELECT * FROM users WHERE email = :email AND password = :password");
$user->execute(['email' => $email, 'password' => sha1($password)]);
$result = $user->fetch();

if($result) {
	$response = array(
		"success" => true,
		"data" => array(
			"userId" => $result["id"],
			"username" => $result["username"],
			"email" => $result["email"]
		),
		"message" => "Logged in successfully."
	);
}else{
	$response = array(
		"success" => false,
		"message" => "Invalid credentials"
	);	
}

echo json_encode($response);