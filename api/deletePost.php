<?php

require "connection.php";

$dbConnection = getConnection();

$response = array();

$postReference 	= $_POST["postReference"];

$currentDate = getCurrentDate();

// UPDATE POST
$updatePostStatement = $dbConnection->prepare("UPDATE posts SET status = 0 WHERE id = :postReference");	

try {

	$updatePostStatement->execute([
		'postReference' => $postReference
	]);

	$response = array(
		"success" => true,
		"message" => "Post updated"
	);

} catch(Exception $e) {
	$response = array(
		"success" => false,
		"message" => $e->getMessage()
	);
}	
echo json_encode($response);