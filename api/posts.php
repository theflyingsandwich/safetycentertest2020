<?php

require "connection.php";

$dbConnection = getConnection();

$response = array();

//GET POSTS
$getPostsStatement = $dbConnection->prepare("SELECT p.*, u.id as user_id, u.username FROM posts p JOIN users u ON p.user_reference = u.id WHERE p.status = 1");	
$getPostsStatement->execute();
try {

	$posts = $getPostsStatement->fetchAll();

	$postsArray = array();

	foreach($posts as $post) {
		$postsArray[] = array(
			"postId" => $post["id"],
			"userReference" => $post["user_reference"],
			"username" => $post["username"],
			"title" => $post["title"],
			"content" => $post["content"],
			"dateCreated" => date("F j, Y | h:i A", strtotime($post["date_created"]))
		);
	} 

	$response = array(
		"success" => true,
		"data" => $postsArray,
		"message" => "Post created"
	);

} catch(Exception $e) {
	$response = array(
		"success" => false,
		"message" => $e->getMessage()
	);
}	
echo json_encode($response);