<?php
date_default_timezone_set("Asia/Manila");

function getConnection() {
	$servername = "localhost";
	$username = "root";
	$password = "root";

	try {
		$conn = new PDO("mysql:host=$servername;dbname=test_blog", $username, $password);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	    return $conn;
	} catch(PDOException $e){
	    return "Connection failed: " . $e->getMessage(); //"error";
	}
}

function getCurrentDate() {
	return date("Y-m-d H:i:s");
}


?>