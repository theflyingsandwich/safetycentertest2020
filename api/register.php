<?php

require "connection.php";

$dbConnection = getConnection();

$response = array();

$username 	= filter_var($_POST["username"], FILTER_SANITIZE_STRING);
$email 		= filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);
$password 	= filter_var($_POST["password"], FILTER_SANITIZE_STRING);

$currentDate = getCurrentDate();

// CHECK IF USERNAME EXISTS

$usernameStatement = $dbConnection->prepare("SELECT * FROM users WHERE username = :username OR email = :email");
$usernameStatement->execute(['username' => $username, 'email' => $email]);
$result = $usernameStatement->fetch();

if($result) {
	$response = array(
		"success" => false,
		"message" => "Username or email exists."
	);
}else{

	// NEW USER
	$newUserStatement = $dbConnection->prepare("INSERT into users (username, email, password, date_created) VALUES(:username, :email, :password, :dateCreated)");	

	try {

		$newUserStatement->execute([
			'username' => $username,
			'email' => $email,
			'password' => sha1($password),
			'dateCreated' => $currentDate
		]);

		$response = array(
			"success" => true,
			"message" => "User created"
		);

	} catch(Exception $e) {
		$response = array(
			"success" => false,
			"message" => $e->getMessage()
		);
	}	
}

echo json_encode($response);